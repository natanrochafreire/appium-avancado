$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("login.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#language: pt"
    }
  ],
  "line": 3,
  "name": "Login de Usuario Melhoria de cenário bonito",
  "description": "",
  "id": "login-de-usuario-melhoria-de-cenário-bonito",
  "keyword": "Funcionalidade"
});
formatter.before({
  "duration": 30612875300,
  "status": "passed"
});
formatter.background({
  "line": 5,
  "name": "Digitar Email",
  "description": "",
  "type": "background",
  "keyword": "Contexto"
});
formatter.step({
  "line": 6,
  "name": "que eu escreva as informações do usuário \"teste@teste.com\"",
  "keyword": "Dado "
});
formatter.match({
  "arguments": [
    {
      "val": "teste@teste.com",
      "offset": 42
    }
  ],
  "location": "LoginSteps.que_eu_escreva_as_informações_do_usuário(String)"
});
formatter.result({
  "duration": 240114700,
  "status": "passed"
});
formatter.scenario({
  "line": 23,
  "name": "Login com sucesso",
  "description": "",
  "id": "login-de-usuario-melhoria-de-cenário-bonito;login-com-sucesso",
  "type": "scenario",
  "keyword": "Cenário",
  "tags": [
    {
      "line": 22,
      "name": "@test-2"
    }
  ]
});
formatter.step({
  "line": 24,
  "name": "escrever os dados da senha \"123456\"",
  "keyword": "Quando "
});
formatter.step({
  "line": 25,
  "name": "clicar no botão Login",
  "keyword": "Então "
});
formatter.match({
  "arguments": [
    {
      "val": "123456",
      "offset": 28
    }
  ],
  "location": "LoginSteps.escrever_os_dados_da_senha(String)"
});
formatter.result({
  "duration": 1031011000,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.clicar_no_botão_Login()"
});
formatter.result({
  "duration": 938180700,
  "status": "passed"
});
});