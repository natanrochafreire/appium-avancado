package com.test.steps;

import com.test.screen.ScreenLogin;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;

public class LoginSteps {

    ScreenLogin login = new ScreenLogin();

    @Dado("^que eu escreva as informações do usuário \"([^\"]*)\"$")
    public void que_eu_escreva_as_informações_do_usuário(String arg1) {

    }


    @Quando("^escrever os dados da senha \"([^\"]*)\"$")
    public void escrever_os_dados_da_senha(String arg1) {
        login.writePass(arg1);

    }


    @Então("^clicar no botão Login$")
    public void clicar_no_botão_Login() {
        login.clickLogin();

    }
}