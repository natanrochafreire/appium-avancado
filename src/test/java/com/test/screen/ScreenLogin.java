package com.test.screen;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.remote.RemoteWebElement;

public class ScreenLogin extends BaseScreen {

    @AndroidFindBy(id = "login_username")
    @iOSFindBy(accessibility = "login_username")
    private RemoteWebElement fieldEmail;

    @AndroidFindBy(id = "login_password")
    @iOSFindBy(accessibility = "login_password")
    private RemoteWebElement fieldPass;

    @AndroidFindBy(accessibility = "email")
    @iOSFindBy(accessibility = "login_username")
    private RemoteWebElement email;

    @AndroidFindBy(accessibility = "senha")
    @iOSFindBy(accessibility = "login_password")
    private RemoteWebElement pass;

    @AndroidFindBy(accessibility = "entrar")
    @iOSFindBy(accessibility = "login_password")
    private RemoteWebElement loginBtn;

    public void writeEmail(String txt){
        email.sendKeys(txt);
    }

    public void writePass(String txt){ pass.sendKeys(txt);}

    public void clickLogin(){ loginBtn.click();}
}
