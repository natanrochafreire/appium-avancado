package com.test;

import cucumber.api.java.Before;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class Hooks {

    private static AppiumDriver<?> driver;
    public static String platform = "";

    public static AppiumDriver<?> validateDriver() throws MalformedURLException {

        platform = System.getProperty("plataforma");

    if(platform.equals("android")) {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("app", "C:\\Users\\Geska e Natan\\IdeaProjects\\appium-com-cucumber\\apps\\app-release.apk");
        capabilities.setCapability("deviceName", "emulator-5554");
        capabilities.setCapability("platformName", "Android");
        driver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), capabilities);

    } else if(platform.equals("ios")){

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("app", "C:\\Users\\Geska e Natan\\IdeaProjects\\appium-com-cucumber\\apps\\app-debug.apk");
        capabilities.setCapability("deviceName", "emulator-5554");
        capabilities.setCapability("platformName", "IOS");
        driver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), capabilities);
        
    } else if (platform.equals("devicefarm")){

        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("browserstack.user", "natanaelfreire_VMYWx6");
        capabilities.setCapability("browserstack.key", "c8A4mRXBuB19kpzr34vo");

        capabilities.setCapability("app", "bs://c4ab52139dff48a492711e6b352fb3b2fdd9d0b7");

        capabilities.setCapability("device", "Google Pixel 3");
        capabilities.setCapability("os_version", "9.0");

        driver = new AndroidDriver(new URL("http://hub.browserstack.com/wd/hub"), capabilities);


    } else {

        System.out.println("Plataforma não encontrada");

    }
        return driver;
    }

    public static AppiumDriver<?> getDriver(){
        return driver;
    }

    public static void quitDriver() {
        if(driver != null) {
            driver.quit();
        }
    }

    @Before
    public static void startProject() throws MalformedURLException {
        if (getDriver() != null){
            getDriver().launchApp();
        } else {
            validateDriver();
        }
    }
}