#language: pt

Funcionalidade: Login de Usuario Melhoria de cenário bonito

  Contexto: Digitar Email
    Dado que eu escreva as informações do usuário "teste@teste.com"

  @test
  Cenário: Login com sucesso
    Quando escrever os dados da senha "123456"

  @test-1
  Esquema do Cenário: : Login com sucesso
    Quando escrever os dados da senha "<senha>"

    Exemplos:
      | senha  |
      | 123456 |
      | 234567 |
      | 345678 |

  @test-2
  Cenário: Login com sucesso
    Quando escrever os dados da senha "123456"
    Então clicar no botão Login